set nocompatible
set history=50
set ignorecase
set smartcase
set hlsearch
set incsearch
set number
set list
set showmatch
syntax on
highlight Comment ctermfg=LightCyan
